TARGET   := confkerndev
TARGET32 := ${TARGET}i386
WARN     := -Wall
DEBUG    := -g
#LEAKTEST := -fsanitize=leak
i386FLAG := -DELFSIZE=32 -m32
CFLAGS   := ${WARN} ${DEBUG} ${LEAKTEST} -fdiagnostics-color=always
LDFLAGS  :=

all: ${TARGET}

i386:
	${CC} ${i386FLAG} ${CFLAGS} -o ${TARGET32} ${TARGET}.c

clean:
	rm -rf *.o ${TARGET} ${TARGET32}

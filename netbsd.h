#if defined(ELFSIZE) && (ELFSIZE == 32)
#define Elf_Ehdr    Elf32_Ehdr
#define Elf_Phdr    Elf32_Phdr
#define Elf_Shdr    Elf32_Shdr
#define Elf_Sym     Elf32_Sym
#else
#define Elf_Ehdr    Elf64_Ehdr
#define Elf_Phdr    Elf64_Phdr
#define Elf_Shdr    Elf64_Shdr
#define Elf_Sym     Elf64_Sym
#endif



/*
 * Configuration data (i.e., data placed in ioconf.c).
 */
struct cfdata {
    const char *cf_name;        /* driver name */
    const char *cf_atname;      /* attachment name */
    short   cf_unit;        /* unit number */
    short   cf_fstate;      /* finding state (below) */
    int *cf_loc;        /* locators (machine dependent) */
    int cf_flags;       /* flags from config */
    const struct cfparent *cf_pspec;/* parent specification */
};
#define FSTATE_NOTFOUND     0   /* has not been found */
#define FSTATE_FOUND        1   /* has been found */
#define FSTATE_STAR     2   /* duplicable */
#define FSTATE_DSTAR        3   /* has not been found, and disabled */
#define FSTATE_DNOTFOUND    4   /* duplicate, and disabled */

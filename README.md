# confkerndev

**Note 2023-02-14** : `confkerndev` is in *pkgsrc-wip* now

The original idea for this tool came from the [mksmolnb](https://gitlab.com/iMil/mksmolnb) by [Emile "iMil" Heitor](https://gitlab.com/iMil), using a GDB script. But why use GDB if you can have a good excuse to develop some code and learn a lot more about ELF along the way? So I did.

This tool allows you to disable (for now) drivers in a binary NetBSD kernel, without having to recompile or specify arguments via the bootloader. You can select which drivers to disable in two ways:

- disable a set of drivers with the `-d` option.
- disable all drivers except those listed with the `-k` option

Used without the `-k` or `-d` options (and `w`), the tool will list device drivers by name along with their state (`cf_fstate`). This tool does not remove any code, it only changes the state of the driver in the kernel configuration (`struct cfdata`).

`confkerndev` now **also works under GNU/Linux**. It has been tested with:
- NetBSD 9.3 on amd64, i386 and sparc64
- FreeBSD 13.1 on amd64
- OpenBSD 7.2 on i386
- GNU/Linux 4.16.5 on amd64

This tool will by default only work with a kernel compiled for the same architecture as the one used to compile it. This means that a `confkerndev` compiled for amd64 will only work with an amd64 kernel. **But you can build a 32-bit version to handle 32-bit kernels (aka i386)**. Just use `make i386` to get a `confkerndevi386` binary :

```
$ uname -srpm
NetBSD 9.3 amd64 x86_64

$ make all i386

$ file confkerndev confkerndevi386
confkerndev:     ELF 64-bit LSB executable, x86-64, version 1 (SYSV), dynamically linked,
   interpreter /usr/libexec/ld.elf_so, for NetBSD 9.3, with debug_info, not stripped
confkerndevi386: ELF 32-bit LSB executable, Intel 80386, version 1 (SYSV), dynamically linked,
   interpreter /usr/libexec/ld.elf_so, for NetBSD 9.3, with debug_info, not stripped

$ file test/netbsd93i386
test/netbsd93i386: ELF 32-bit LSB executable, Intel 80386, version 1 (SYSV),
statically linked, for NetBSD 9.3, with debug_info, not stripped
```

This will not work:

```
$ ./confkerndev -v -i test/netbsd93i386 -w -d umass
confkerndev: incorrect ELF header or truncated file (e_shoff)
confkerndev: Unable to load and analyze ELF file

$ ./confkerndev  -i test/netbsd93i386 | grep umass
confkerndev: incorrect ELF header or truncated file (e_shoff)
confkerndev: Unable to load and analyze ELF file
```

But this will:

```
$ ./confkerndevi386  -i test/netbsd93i386 | grep umass
umass  FSTATE_STAR (2)

$ ./confkerndevi386 -v -i test/netbsd93i386 -w -d umass
disabling umass (FSTATE_DSTAR)

$ ./confkerndevi386  -i test/netbsd93i386 | grep umass
umass  FSTATE_DSTAR (3)
```

This is however not a cross-platform tool and it will probably only work between amd64 and i386.

Note: the `-w` option is required to modify the ELF file (mmapping with `PROT_WRITE`). This is a kind of security to avoid mistakes.

Note 2: **Do not use this tool as root** and especially not on `/netbsd` directly! You can seriously break everything.

Some of the code for this tool was inspired by OpenBSD's `config` command.

Here are some examples :

- All disabled except a handful of drivers (and be verbose about it):

```
$ cp /netbsd ./elffile
$ ./confkerndev -v -i elffile -w -k mainbus -k cpu -k acpicpu -k ioapic -k pci -k isa -k pcdisplay -k wsdisplay -k com -k virtio -k ld -k vioif
disabling spkr (FSTATE_DSTAR)
disabling spkr (FSTATE_DSTAR)
disabling audio (FSTATE_DSTAR)
disabling audio (FSTATE_DSTAR)
disabling midi (FSTATE_DSTAR)
disabling midi (FSTATE_DSTAR)
disabling hdaudio (FSTATE_DSTAR)
disabling hdafg (FSTATE_DSTAR)
[...]
disabling hvs (FSTATE_DSTAR)
disabling hvheartbeat (FSTATE_DSTAR)
disabling hvshutdown (FSTATE_DSTAR)
disabling hvtimesync (FSTATE_DSTAR)
```

- list what's left:

```
$ ./confkerndev -i elffile | grep FSTATE_STAR
ld  FSTATE_STAR (2)
ld  FSTATE_STAR (2)
ld  FSTATE_STAR (2)
ld  FSTATE_STAR (2)
ld  FSTATE_STAR (2)
ld  FSTATE_STAR (2)
ld  FSTATE_STAR (2)
ld  FSTATE_STAR (2)
ld  FSTATE_STAR (2)
ld  FSTATE_STAR (2)
ld  FSTATE_STAR (2)
ld  FSTATE_STAR (2)
com  FSTATE_STAR (2)
com  FSTATE_STAR (2)
com  FSTATE_STAR (2)
com  FSTATE_STAR (2)
com  FSTATE_STAR (2)
com  FSTATE_STAR (2)
virtio  FSTATE_STAR (2)
vioif  FSTATE_STAR (2)
wsdisplay  FSTATE_STAR (2)
wsdisplay  FSTATE_STAR (2)
wsdisplay  FSTATE_STAR (2)
cpu  FSTATE_STAR (2)
acpicpu  FSTATE_STAR (2)
ioapic  FSTATE_STAR (2)
pci  FSTATE_STAR (2)
pci  FSTATE_STAR (2)
pci  FSTATE_STAR (2)
```

- Start over and disable `ugen`:

```
$ cp /netbsd elffile
$ ./confkerndev -i elffile -d ugen -w
```

- Check `ugen` status:

```
$ ./confkerndev -i elffile | grep ugen
ugen  FSTATE_DSTAR (3)
ugen  FSTATE_DSTAR (3)
ugenif  FSTATE_STAR (2)
ugenif  FSTATE_STAR (2)
ugenif  FSTATE_STAR (2)
ugenif  FSTATE_STAR (2)
ugenif  FSTATE_STAR (2)
ugenif  FSTATE_STAR (2)
ugensa  FSTATE_STAR (2)
```

- Start over with i386 version and disable with a file of drivers to keep:

```
$ cp test/netbsd93i386 elffile

$ cat imillist
mainbus
cpu
acpicpu
ioapic
pci
isa
pcdisplay
wsdisplay
com
virtio
ld
vioif

$ ./confkerndevi386 -v -i elffile -K imillist -w
12 driver names loaded from imillist.
disabling audio (FSTATE_DSTAR)
disabling audio (FSTATE_DSTAR)
disabling midi (FSTATE_DSTAR)
[...]
disabling hvshutdown (FSTATE_DSTAR)
disabling hvtimesync (FSTATE_DSTAR)
disabling glxsb (FSTATE_DSTAR)

$ ./confkerndevi386 -ci elffile | grep FSTATE_DSTAR | wc -l
     663

$ ./confkerndevi386 -ci elffile | grep FSTATE_STAR | wc -l
      32
```

Tip:
You can get the list of device drivers currently used in the kernel with:

```
$ sudo sh -c 'drvctl -lt | sed "s/ //g" | sed "s/[0-9]//" | sort | uniq '
acpi
acpiacad
acpibat
acpibut
acpicpu
[...]
usb
wd
wm
wsdisplay
wskbd
wsmouse
```

and use it as a base to build a file that you will use with the `-K` option. But **be careful** because any device (USB in particular) not present at this time will no longer be supported with the modified kernel. This includes, for example, `umass` for USB storage media or USB/serial adapters (`ucom`).

TODO :
- ~~Run independently of the target platform (change an i386 kernel on an amd64 system for example (other OS ?)).~~ **DONE** (somehow)
- ~~use a configuration file for the list of drivers~~ **DONE**
- ~~accept other syntax to specify the list of drivers to disable or keep (multiple occurrences of `-d` or `-k` is a pain)~~ **DONE** (load list from file does that)
- allow to enable drivers as well (not sure if this is a good idea)
- write the manpage (I'm not motivated at all)

/*
 * Copyright (c) 2023 Denis Bodor
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE NETBSD FOUNDATION, INC. AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE FOUNDATION OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * -------
 *
 * Some of the code used here comes from OpenBSD config sources.
 * More precisely, the adjust() function and some checks done in loadkernel().
 *
 */

#include <elf.h>
#include <err.h>
#include <fcntl.h>
#include <nlist.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#if defined(__NetBSD__)
#include <sys/device.h>
#else
#include "netbsd.h"
#endif

#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>

#include "colors.h"

#define IS_ELF(ehdr) ((ehdr).e_ident[EI_MAG0] == ELFMAG0 && \
                      (ehdr).e_ident[EI_MAG1] == ELFMAG1 && \
                      (ehdr).e_ident[EI_MAG2] == ELFMAG2 && \
                      (ehdr).e_ident[EI_MAG3] == ELFMAG3)

struct nlist nl[] = {
    { "cfdata" },
    { NULL },
};

caddr_t     ptr, rest, pre;
Elf_Ehdr    elf_ex;
Elf_Phdr    *elf_phdr;
Elf_Shdr    *elf_shdr;
char        *elf_total;
char        *elf_shstrtab;
off_t       elf_size;
char        *elf_strtab;
Elf_Sym     *elf_sym;

char **namelist = NULL;
size_t listlength = 0;

char *ifilename = NULL;
char *inamesfile = NULL;

int
loadkernel(char *file, int writemode)
{
	struct stat file_status;
	int fdi;
	int openflags = O_RDONLY;
	int mmapflags = PROT_READ;

	if (writemode) {
		openflags = O_RDWR;
		mmapflags = PROT_READ | PROT_WRITE;
	}

	if (stat(file, &file_status) == -1 ) {
		warn("Error getting stats from file");
		return -1;
	}

	elf_size = file_status.st_size;

	if ((fdi = open(file, openflags)) == -1) {
		warn("Error Opening file");
		return -1;
	}

	if ((elf_total = mmap(0, elf_size, mmapflags, MAP_SHARED, fdi, 0)) == MAP_FAILED) {
		close(fdi);
		warn("Error mmapping the file in");
		return -1;
	}

	if (read(fdi, (char *)&elf_ex, sizeof(elf_ex)) != sizeof(elf_ex)) {
		warn("can't read elf header");
		return -1;
	}

	close(fdi);

    if (!IS_ELF(elf_ex)) {
        warnx("bad elf magic");
		return -1;
	}

	// check if Program Header offset from Elf_Ehdr struct (elf_ex.e_phoff) is in file range (elf_size)
    if (elf_ex.e_phoff > (size_t)elf_size) {
        warnx("incorrect ELF header or truncated file (e_phoff)");
		return -1;
	}
	// set address of Program Header Table in elf_total[]
    elf_phdr = (Elf_Phdr *)&elf_total[elf_ex.e_phoff];

	// check if Section Header offset Elf_Ehdr struct (elf_ex.e_shoff) is in file range (elf_size)
    if (elf_ex.e_shoff > (size_t)elf_size) {
        warnx("incorrect ELF header or truncated file (e_shoff)");
		return -1;
	}
	// set address of Section Header in elf_total[]
    elf_shdr = (Elf_Shdr *)&elf_total[elf_ex.e_shoff];

	// check if String table last entry is in file range
    if ((char *)&elf_shdr[elf_ex.e_shstrndx] +
        sizeof(elf_shdr[elf_ex.e_shstrndx]) > elf_total + (size_t)elf_size) {
		warnx("incorrect ELF header or truncated file (elf_shdr[elf_ex.e_shstrndx]).");
		return -1;
	}

    if ((char *)&elf_shdr[elf_ex.e_shstrndx].sh_offset +
        sizeof(elf_shdr[elf_ex.e_shstrndx].sh_offset) >
        elf_total + (size_t)elf_size) {
		warnx("incorrect ELF header or truncated file (sh_offset)");
		return -1;
	}

    elf_shstrtab = &elf_total[elf_shdr[elf_ex.e_shstrndx].sh_offset];

	return 0;
}

caddr_t
adjust(caddr_t x)
{
    int i;
    Elf_Shdr *s;
    unsigned long y = 0;

    s = elf_shdr;

    for (i = 0; i < elf_ex.e_shnum; i++) {
        if (s[i].sh_addr == 0)
            continue;
        if (((unsigned long)x >= s[i].sh_addr) &&
            ((unsigned long)x < (s[i].sh_addr+s[i].sh_size))) {
            y = (unsigned long)&elf_total[(unsigned long)x -
                s[i].sh_addr + s[i].sh_offset];
            break;
        }
    }

    return((caddr_t)y);
}

struct cfdata *
getsymval(char *symname)
{
    int nbrsym = 0;

    // loop on sections
    for (int i = 0; i <= elf_ex.e_shstrndx; i++) {
        if (elf_shdr[i].sh_type == SHT_SYMTAB) {
            nbrsym = elf_shdr[i].sh_size/sizeof(Elf_Sym);
			// set symbol table address
            elf_sym = (Elf_Sym *)&elf_total[elf_shdr[i].sh_offset];
        }
        if (elf_shdr[i].sh_type == SHT_STRTAB) {
            if (strcmp((char *)&elf_shstrtab[elf_shdr[i].sh_name], ".strtab") == 0)
				// set strinf table address
                elf_strtab = &elf_total[elf_shdr[i].sh_offset];
        }
    }

	// look for symbol
    for (int i = 0; i < nbrsym; i++) {
        if (strcmp((char *)&elf_strtab[elf_sym[i].st_name], symname) == 0) {
            return (struct cfdata*)adjust((caddr_t)elf_sym[i].st_value);
        }
    }

    return NULL;
}

void
printhelp()
{
	printf("confkerndev v0.0.1\n");
    printf("Copyright (c) 2023 - Denis Bodor\n");
	printf(" -i infile     use this ELF file as input\n");
	printf(" -k name       disable everything except 'name' (can be used multiple time)\n");
	printf(" -d name       disable 'name' (can be used multiple time)\n");
	printf(" -w            enable write mode (mandatory for -k, -d, -K or -D)\n");
	printf(" -c            color output\n");
	printf(" -l            list all driver names present without their status (for creating a base file)\n");
	printf(" -K file       disable everything except what is listed in 'file'\n");
	printf(" -D file       disable what is listed in 'file'\n");
	printf(" -v            be verbose (can be used multiple times to increase verbosity level)\n");
	printf(" -h            show this help\n");
	printf("\nRun with only the -i option, confkerndev will list the drivers with their status.\n");
}

void
add2list(char *name)
{
    if (!(namelist = realloc(namelist, sizeof(char *) * (listlength + 1) ))) {
        err(EXIT_FAILURE, "list malloc failed!");
    }
    namelist[listlength] = name;
    listlength++;
}

int
matchlist(char *name)
{
    for (unsigned int i = 0; i < listlength; i++) {
        if (strcmp(namelist[i], name) == 0)
                return 1;
    }
    return 0;
}

void
cleanlist()
{
    for (unsigned int i = 0; i < listlength; i++) {
        free(namelist[i]);
    }
    free(namelist);
}

void
printlist()
{
    for (unsigned int i = 0; i < listlength; i++) {
        printf("%u: %s\n", i, namelist[i]);
    }
}

int
listfromfile(char *filename)
{
	FILE *fp;
	char *line = NULL;
	size_t len = 0;
	ssize_t readlen;
	int count = 0;

	if ((fp = fopen(filename, "r")) == NULL) {
		warn("Unable to open driver name file");
		return -1;
	}

	while ((readlen = getline(&line, &len, fp)) != -1) {
		if (readlen > 0) {
			if (line[readlen-1] == '\n')
				line[readlen-1] = 0;
			if (strlen(line) > 0) {
				add2list(strdup(line));
				count++;
			}
		}
	}

	fclose(fp);

	if (line)
        free(line);

	return count;
}

void
addcolor(int state)
{
	if(state == FSTATE_FOUND || state == FSTATE_STAR)
		printf(GREEN);
	else
		printf(RED);
}

void
badarg(char c)
{
    fprintf(stderr,"option requires an argument -- %c\n", c);
	printhelp();
    exit(EXIT_FAILURE);
}

int
main (int argc, char**argv)
{
	int retopt;
	int optcolor = 0;
	int optwrite = 0;
	int optverb = 0;
	int optlist = 0;
	int optkeep = 0;
	int optdisfile = 0;
	int optkeepfile = 0;
	int optdisable = 0;
	int ret;

#if !defined(__linux__)
	int nbrsym;
#endif

	struct cfdata *confdata;

	while ((retopt = getopt(argc, argv, "i:k:d:K:D:wclvh")) != -1) {
		switch (retopt) {
		case 'i':
			ifilename = strdup(optarg);
			if (optarg[0] == '-')
				badarg(retopt);
			break;
		case 'k':
			if (optdisable || optdisfile || optkeepfile)
				errx(EXIT_FAILURE, "Cannot use -d with -k -D or -K!");
			if (optarg[0] == '-')
				badarg(retopt);
			optkeep = 1;
			add2list(strdup(optarg));
			break;
		case 'd':
			if (optkeep || optdisfile || optkeepfile)
				errx(EXIT_FAILURE, "Cannot use -k with -d -D or -K!");
			if (optarg[0] == '-')
				badarg(retopt);
			optdisable = 1;
			add2list(strdup(optarg));
			break;
		case 'K':
			if (optdisable || optdisable || optdisfile)
				errx(EXIT_FAILURE, "Cannot use -K with -k -d or -D!");
			if (optarg[0] == '-')
				badarg(retopt);
			optkeepfile = 1;
			inamesfile = strdup(optarg);
			break;
		case 'D':
			if (optkeep || optdisable || optkeepfile)
				errx(EXIT_FAILURE, "Cannot use -D with -k -d or -K!");
			if (optarg[0] == '-')
				badarg(retopt);
			optdisfile = 1;
			inamesfile = strdup(optarg);
			break;
		case 'w':
			optwrite = 1;
			break;
		case 'c':
			optcolor = 1;
			break;
		case 'l':
			optlist = 1;
			break;
		case 'v':
			optverb++;
			break;
		case 'h':
			printhelp();
			return EXIT_SUCCESS;
			break;
		default:
			printhelp();
			return EXIT_FAILURE;
		}
	}

	if (!ifilename) {
		printhelp();
		return EXIT_FAILURE;
	}

	if ((optkeep || optdisable || optdisfile || optkeepfile) && optlist)
		errx(EXIT_FAILURE, "Cannot list and modify at the same time!");

	if ((optkeep || optdisable || optdisfile || optkeepfile) && !optwrite)
		errx(EXIT_FAILURE, "You must use -w (write enable) with -d, -k, -D or -K!");

	if (optdisfile || optkeepfile) {
		ret = listfromfile(inamesfile);

		if (ret < 0)
			errx(EXIT_FAILURE, "Unable to load driver name list.");

		if (ret < 1)
			errx(EXIT_FAILURE, "No driver names were loaded.");

		if (optverb)
			printf("%d driver names loaded from %s.\n", ret, inamesfile);
	}

	if (loadkernel(ifilename, optwrite) != 0) {
		if (ifilename)
			free(ifilename);
#if defined(__x86_64__)
		if (elf_ex.e_ident[EI_CLASS] == ELFCLASS32) {
			fprintf(stderr, "Tip: You are trying to manipulate a 32-bit ELF binary and confkerndev is compiled for a 64-bit architecture.\n");
			fprintf(stderr, "     Try again with a 32-bit compiled version of this tool (make i386).\n");
		}
#endif
		errx(EXIT_FAILURE, "Unable to load and analyze ELF file");
	}

#if defined(__linux__)
	confdata = getsymval("cfdata");
	if (!confdata) {
		if (ifilename)
			free(ifilename);
		if (munmap(elf_total, elf_size) == -1) {
			err(EXIT_FAILURE, "Error un-mmapping inputfile");
		}
		if (elf_ex.e_ident[EI_OSABI] != ELFOSABI_NETBSD)
			fprintf(stderr, "Warning: This is not a NetBSD binary file. What are you doing?\n");
		errx(EXIT_FAILURE, "Error searching for required symbols.");
	}
#else
	nbrsym = nlist(ifilename, nl);

	if (nbrsym != 0) {
		if (ifilename)
			free(ifilename);
		if (munmap(elf_total, elf_size) == -1) {
			err(EXIT_FAILURE, "Error un-mmapping inputfile");
		}
		if (elf_ex.e_ident[EI_OSABI] != ELFOSABI_NETBSD)
			fprintf(stderr, "Warning: This is not a NetBSD binary file. What are you doing?\n");
		errx(EXIT_FAILURE, "Error searching for required symbols.");
	}

	confdata = (struct cfdata *)(adjust((caddr_t)nl[0].n_value));
#endif

	while (adjust((char *)confdata->cf_name) != NULL) {
		if (optkeep || optkeepfile) {
			// disable everything except what is in the list
			if (!matchlist(adjust((char *)confdata->cf_name))) {
				if (optverb > 1)
					printf("disabling %s (FSTATE_DSTAR)\n", adjust((char *)confdata->cf_name));
				confdata->cf_fstate = FSTATE_DSTAR;
			}
		} else if (optdisable || optdisfile) {
			// disable what is in the list
			if (matchlist(adjust((char *)confdata->cf_name))) {
				if (optverb > 1)
					printf("disabling %s (FSTATE_DSTAR)\n", adjust((char *)confdata->cf_name));
				confdata->cf_fstate = FSTATE_DSTAR;
			}
		} else {
			// just print
			printf("%s", adjust((char *)confdata->cf_name));
			if (!optlist) {
				if (optcolor)
					addcolor(confdata->cf_fstate);
				switch(confdata->cf_fstate) {
				case FSTATE_NOTFOUND:
					printf(" FSTATE_NOTFOUND (%d)", confdata->cf_fstate);
					break;
				case FSTATE_FOUND:
					printf(" FSTATE_FOUND (%d)", confdata->cf_fstate);
					break;
				case FSTATE_STAR:
					printf(" FSTATE_STAR (%d)", confdata->cf_fstate);
					break;
				case FSTATE_DSTAR:
					printf(" FSTATE_DSTAR (%d)", confdata->cf_fstate);
					break;
				case FSTATE_DNOTFOUND:
					printf(" FSTATE_DNOTFOUND (%d)", confdata->cf_fstate);
					break;
				default:
					printf(" ????? (%d)", confdata->cf_fstate);
					break;
				}
				if (optcolor)
					printf(RESET);
			}
			printf("\n");
		}

		confdata++;
	}

	if (inamesfile)
		free(inamesfile);

	if (ifilename)
		free(ifilename);

	if (munmap(elf_total, elf_size) == -1) {
		err(EXIT_FAILURE, "Error un-mmapping inputfile");
	}

	if (optkeep || optdisable || optkeepfile || optdisfile) {
		cleanlist();
	}

	return EXIT_SUCCESS;
}
